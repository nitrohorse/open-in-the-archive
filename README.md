# Open in the Archive

[![Downloads](https://img.shields.io/amo/users/open-in-the-archive.svg)](https://addons.mozilla.org/en-US/firefox/addon/open-in-the-archive/)

Web extension that opens links (in the background) in archive sites such as [the Wayback Machine](https://web.archive.org/) or [Archive.is](https://archive.is/).

## Download
* https://addons.mozilla.org/en-US/firefox/addon/open-in-the-archive/

## Develop Locally
* Clone the repo
* Install tools:
	* [Node.js](https://nodejs.org)
    * [nvm](https://github.com/nvm-sh/nvm)
* Use correct Node version:
    * `nvm use`
* Install dependencies:
	* `npm i`
* Run add-on in isolated Firefox instance using [web-ext](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext) (open the [Browser Toolbox](https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox) for console logging):
	* `npm start`
* Package for distribution:
	* `npm run bundle`

## Resources

<a target="_blank" href="https://icons8.com/icons/set/bank-building">Bank Building icon</a> by <a target="_blank" href="https://icons8.com">Icons8</a>