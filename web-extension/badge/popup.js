'use strict'

const listenForClicks = () => {
	document.addEventListener('click', (e) => {
		extensionApi.tabs.query({
			currentWindow: true,
			active: true
		}, (tabs) => {
			const tabUrl = new URL(tabs[0].url)
			const archiveId = e.target.id

			if (archiveId === 'all') {
				window.archives.forEach((archive) => {
					extensionApi.tabs.create({
						active: false,
						url: `${archive.url}${tabUrl}`
					})
				})
			} else {
				const archive = window.archives.filter(a => a.id === archiveId)[0]
				extensionApi.tabs.create({
					active: false,
					url: `${archive.url}${tabUrl}`
				})
			}
			window.close();
		})
	})
}

listenForClicks()
