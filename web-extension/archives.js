window.archives = [
	{
		id: 'archive.org',
		title: 'Open in the Wayback Machine',
		url: 'https://web.archive.org/web/2/'
	},
	{
		id: 'archive.is',
		title: 'Open in Archive.is',
		url: 'https://archive.is/newest/'
	}
]
