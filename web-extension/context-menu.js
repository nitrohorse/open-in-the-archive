'use strict'

window.archives.forEach((archive) => {
	extensionApi.contextMenus.create({
		id: archive.id,
		title: archive.title,
		contexts: ['link']
	})
})

extensionApi.contextMenus.create({
	id: 'all',
	title: 'Open in All Archives',
	contexts: ['link']
})

extensionApi.contextMenus.onClicked.addListener((info, tab) => {
	const archiveId = info.menuItemId

	if (archiveId === 'all') {
		window.archives.forEach((archive) => {
			extensionApi.tabs.create({
				active: false,
				url: `${archive.url}${info.linkUrl}`
			})
		})
	} else {
		const archive = window.archives.filter(a => a.id === archiveId)[0]
		extensionApi.tabs.create({
			active: false,
			url: `${archive.url}${info.linkUrl}`
		})
	}
})
